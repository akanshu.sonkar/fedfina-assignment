pkill -f -9 gunicorn
pkill -f -9 python3
export LANG=C.UTF-8
#nohup uwsgi --http :9090 --wsgi-file server.py
export BOT_ENV="dev"
gunicorn --log-level debug  --workers=6 --bind 0.0.0.0:5002 server:app
