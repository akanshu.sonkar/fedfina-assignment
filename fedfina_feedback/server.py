from flask import Flask, request
import os.path
import json
from logger_config.logger import get_logger
from db.tracker_store_connection import load_config, MongoDB, RedisDB
import pytz
from datetime import datetime, timezone
from datetime import date, timedelta

from utils.text_format import color
from raven import Client
from threading import Thread
import requests




logger = get_logger(__name__)
app = Flask(__name__)

mongo_conn = MongoDB()
redis_db_obj = RedisDB()

bot_config = load_config()

## colors and text format
red = color.RED
blue = color.BLUE
cyan = color.CYAN
darkcyan = color.DARKCYAN
green = color.GREEN
purple= color.PURPLE
yellow = color.YELLOW
bold = color.BOLD
underline = color.UNDERLINE
end = color.END

client = Client(bot_config["raven"]["url"])

IST = pytz.timezone('Asia/Kolkata')

with open("configs/templates.json", "r", encoding="utf-8") as temp:
    templates = json.load(temp)


def get_iso_date(date):
    return f"ISODate({date.isoformat()})"

def send_sms(**kwargs):
    phone_number=kwargs.get("phone_number")
    language=kwargs.get("language")
    logger.info("received phone_number---->"+str(phone_number))
    logger.info("received language---->"+str(language))
    url = "https://tst.fedfina.com/api/send2sms/"

    payload = json.dumps({
    "token": "vlrsIBj4JQs3kYSXB/W0n46fcf/UUrqapOovnWq4qQg",
    "method": "update_KYC",
    "prams": {
        "mobile_no": phone_number,
        "message": "Dear Customer, As informed over the call due to regulatory requirements, regular KYC updation for our customers is mandatory and your Re-KYC is due. You are requested to submit the latest KYC documents at your nearest Fedfina branch at the earliest. Alternatively, you can email the Re-KYC documents to ReKYC@fedfina.com.",
        "lang": "English"
    }
    })
    headers = {
    'Content-Type': 'application/json',
    'Cookie': 'wires=b32cda6300928e12e491173355e68d36'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    return response.text

    
@app.route("/initial_message", methods=["GET"])
def initial_message():
    try:
        logger.info('Argument Recieved are: '+str(request.args))
        logger.info('Mobile Number recieved is: '+str(request.args['mobile']))
        logger.info('Mobile Number recieved is: '+str(request.args['sender_id']))
        mobile = int(request.args['mobile'])
        sender_id = str(request.args['sender_id'])
        _id = str(request.args['_id'])
        
        if "_id" in request.args and _id: 
            q1={'_id' : _id} 
        else:
            q1={'phone_number' : mobile}

        q2 = {"_id": 0}
        input_collection = mongo_conn.mongo_get_collection_data(col_type="input",q1=q1,q2=q2)
        logger.info('Retrived collection: '+str(input_collection))

        
        if input_collection is not None:
            language = input_collection["language"]
            # due_amount = input_collection["due_amount"]
            # bot_type=input_collection.get("bot_type")
            
            language = language.lower()
            logger.info(f"{green} language is {language} {end}")
            
            ## language selection
            language_dict= {
                "kannada" : "kan",
                "english" : "eng",
                "hindi" : "hin",
                "tamil" : "tam",
                "telugu" : "tel",
                "marathi" : "mar",
                "gujrati" : "guj"
            }
            
            lang = language_dict.get(language,"eng")
            key  = "initial_message_" + lang
            
            logger.info(f"{green} language key is {lang} {end}")
            logger.info(f"{green} key is {key} {end}")
            
            message = templates[key]
            # message = message.replace("due_amount",str(due_amount))
            logger.info("Initial Message:"+str(message))

            response_json = {
                        "text":message,
                        "keys":[]
                    }

            logger.info(response_json)
            logger.info("Response is : "+str(message))

            record = {"customerCRTId" : str(sender_id)}
            is_updated = mongo_conn.mongo_update(col_type="input", record=record, upd_cond=q1)

    except Exception as e:
        logger.exception("Exception!! When inserting the new details to DB : " +str(e))
        key = ""
        message = templates[key]

        response_json = {
                    "text":message,
                    "keys":[]
                }
        logger.info("Response is : "+str(message))

    return json.dumps(response_json)


@app.route("/call_status", methods=["POST"])
def call_status():
    logger.info("Entered call status function")
    try:
        insert_status = "FAILURE"

        call_status_main_dict = json.loads(request.data.decode('utf-8'))
        logger.info("Call status is : "+str(call_status_main_dict))
        
        if "call_status" in call_status_main_dict:
            call_status_dict = call_status_main_dict["call_status"]        
            
            if "callStatus" in call_status_dict and "customerPhone" in call_status_dict and "callEndTime" in call_status_dict:
                
                phone_number = int(call_status_dict["customerPhone"])
                sender_id = str(call_status_dict["customerCRTId"])
                
                logger.info("Updating the call status for : "+str(phone_number))
                logger.info("Updating the call status for : "+str(sender_id))
                
                current_date_time = datetime.now(IST)
                todays_date_time = current_date_time.strftime("%Y/%m/%d %H:%M:%S")

                last_triggered_date = datetime.strptime(todays_date_time,'%Y/%m/%d %H:%M:%S')
                last_triggered_date = get_iso_date(last_triggered_date)

                cTime = datetime.now(IST)
                callTime = get_iso_date(cTime)

                logger.info("call status dict:"+str(call_status_dict))

                try:
                    if call_status_dict["dstPhone"] != None:
                        dstPhone = call_status_dict["dstPhone"]
                    else:
                        dstPhone = ''
                except Exception as e:
                    logger.info("Error in dstPhone taking: Exception"+ str(e)) 
                    dstPhone = ''  

                try:
                    if call_status_dict["flow_id"] != None:
                        flow_id = call_status_dict["flow_id"]
                    else:
                        flow_id = ''
                except Exception as e:
                    logger.info("Error in flow_id taking: Exception: "+ str(e)) 
                    flow_id = ''  
                
                try:
                    if call_status_dict["callEndTime"] != None and call_status_dict["callEndTime"] != '':
                        callEndTime = call_status_dict["callEndTime"][:19]
                        callEndTime = datetime.strptime(callEndTime,'%Y/%m/%d %H:%M:%S').\
                            astimezone(pytz.timezone('Asia/Kolkata')) - timedelta(hours=5,minutes=30)
                        callEndTime = get_iso_date(callEndTime)

                        # Have to check the in-consistency in the date splitting
                        date_split = call_status_dict["callEndTime"].split(" ")[0].split("/")
                        new_date = date_split[2]+"/"+date_split[1]+"/"+date_split[0]
                    else:
                        callEndTime = ''
                except Exception as e:
                    logger.info("Error in callEndTime taking: Exception: "+ str(e)) 
                    callEndTime = '' 
                    new_date = ''      
                
                try:
                    if call_status_dict["callConnectedTime"] != None and call_status_dict["callConnectedTime"] != '':
                        callConnectedTime = call_status_dict["callConnectedTime"][:19]
                        callConnectedTime = datetime.strptime(callConnectedTime,'%Y/%m/%d %H:%M:%S').\
                            astimezone(pytz.timezone('Asia/Kolkata')) - timedelta(hours=5,minutes=30)
                        callConnectedTime = get_iso_date(callConnectedTime)
                    else:
                        callConnectedTime = ''
                except Exception as e:
                    logger.info("Error in callConnectedTime taking: Exception: "+str(e))
                    callConnectedTime = ''
                
                call_status = call_status_dict["callStatus"]

                # changes related to new keys
                upload_call_info = {
                    'phone_number':phone_number,
                    'call_status':call_status_dict["callStatus"],
                    'callConnectedTime' : callConnectedTime,
                    'callEndTime' : callEndTime,
                    'customerCRTId' : str(call_status_dict["customerCRTId"]),
                    'srcPhone': call_status_dict["srcPhone"],
                    'ringingTime': int(call_status_dict["ringingTime"]), 
                    'setupTime': int(call_status_dict["setupTime"]),
                    'callType':call_status_dict['callType'],
                    'tempDate':new_date, 
                    'callTime':callTime,
                    'flow_id':flow_id,
                    'dstPhone':dstPhone,
                    "last_triggered_date":last_triggered_date
                    }
               
                q1 = {"customerCRTId":sender_id}
                q2 = {"_id":0} #Column filter condition
                call_trigger_count = mongo_conn.mongo_get_collection_data(col_type="input",q1=q1,q2=q2) 
                logger.debug("Extracted value of call trigger count inside redis data None condition --> "+ str(call_trigger_count))

                if call_status == "ANSWERED":
                    sts = call_status
                    call_trigger_count["sequence_number"] +=1
                    call_trigger_count["answered_seq"] +=1
                    sms_status=send_sms(phone_number=phone_number,language=call_trigger_count.get("language"))
                    logger.info("send sms status ------>"+str(sms_status))
                else:
                    sts = call_status
                    call_trigger_count["sequence_number"] +=1
                    call_trigger_count["no_answer_seq"] +=1

                

                update_seq = {
                    "sequence_number":int(call_trigger_count["sequence_number"]),
                    "answered_seq":int(call_trigger_count["answered_seq"]),
                    "no_answer_seq":int(call_trigger_count["no_answer_seq"]),
                    "customerCRTId":str(sender_id),
                    "sts":sts,
                    "setupTime": int(call_status_dict["setupTime"]),
                    "customerCRTId": str(sender_id),
                    "last_triggered_date":last_triggered_date,
                    "call_status":call_status
                }
                updated_dict = {**call_trigger_count, **update_seq, **upload_call_info}
                logger.debug("Updating input collecton with : " + str(updated_dict))

                upd_cond = {"customerCRTId":sender_id}
                is_updated = mongo_conn.mongo_update(col_type="input",record=updated_dict,upd_cond=upd_cond)
                if is_updated is True:
                    logger.info("Updated the input collection with the update sequence")
                else:
                    logger.info(" Failed to update in the input collection")

                complete_dict = {**updated_dict, **upload_call_info}
                # send_sms(**complete_dict)

                # Inserted updated record to mongo output collection
                is_inserted = mongo_conn.mongo_insert(col_type="output",record=complete_dict)
                if is_inserted is True:
                    insert_status = "SUCCESS"
                    logger.info("INSERTED IN REPORT DB ")
                else:
                    logger.info("Failed to insert in Report DB. Please see logs for errors ")

            else:
                logger.error("Few keys missing in the call status dictionary!")
                
        else:
            logger.error("Call status dictionary not recieved!")

        

    except Exception as e:
        logger.exception("Exception while updating call status")
    
    return insert_status
    
if __name__ == '__main__':
    app.secret_key = "123"
    app.run(host='0.0.0.0',port=5002)