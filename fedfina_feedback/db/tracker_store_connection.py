import sys
sys.path.append('../')

import os
import redis
import yaml
import pickle
from utils.text_format import color
from logger_config.logger import get_logger
from rasa_sdk import Tracker
import json
import requests
from raven import Client
from datetime import datetime

# name of the module for logging 
logger = get_logger(__name__) 

## colors and text format
red = color.RED
blue = color.BLUE
cyan = color.CYAN
darkcyan = color.DARKCYAN
green = color.GREEN
purple= color.PURPLE
yellow = color.YELLOW
bold = color.BOLD
underline = color.UNDERLINE
end = color.END

def load_config():
    conf = ""
    try:
        environment = os.environ["BOT_ENV"]
        logger.info("Bot Environment is :"+str(environment))
        if environment == "prod":
            endpoint_filename = "endpoints.yml"                
        else:
            endpoint_filename = "endpoints_dev.yml" 
        with open('configs/'+endpoint_filename, 'r') as f:
            conf = yaml.load(f, Loader=yaml.FullLoader) 
    
    except Exception as e:
        logger.exception("Error loading the configuration from the endpoints.yml "+str(e))
    return conf


gen_config = load_config()
client = Client(gen_config["raven"]["url"])

headers = {
        'app': 'mongo-db-layer',
        'Content-Type': 'application/json'
    }

class MongoDB:

    def __init__(self):

        try:
        
            self._database_config = load_config()
            environment = self._database_config["server"]["environment"]
            self._mongo_db = self._database_config["mongo_api"]["db_name"]

            mongo_host = self._database_config["mongo_api"]["url"]
            mongo_port = self._database_config["mongo_api"]["port"]
            self.input_col=self._database_config["mongo_api"]["input_collection"]
            self.output_col=self._database_config["mongo_api"]["output_collection"]

            if environment == "prod":
                mongo_port =  os.environ["mongo_port"]
                mongo_host =  os.environ["mongo_host"] + ":" + mongo_port
                self.mongo_url = "http://"+str(mongo_host)
            else:
                self.mongo_url = "http://"+str(mongo_host)+":"+str(mongo_port)
            
        except Exception as e:
            logger.exception("Exception while loading the config-->"+ str(e))

    def mongo_file_count(self,col_type="",q1=""):
        ''' This function will return the count of the record present in collection'''
        if col_type == "input":
            COL = self.input_col
        elif col_type == "output" or col_type == "report":
            COL = self.output_col
        elif col_type == "voice_blast_report":
                COL = self.voice_blast_col
        else:
            logger.info(red+"Please mention the collection type input or output"+end)
            exit()
        mongo_url = self.mongo_url+"/"+"count"
        logger.debug("URL:"+str(mongo_url))
        payload={
                    "db":self._mongo_db,
                    "collection" : COL,
                    "dict_query":q1                    
                }
        li = ["last_triggered_date","callConnectedTime","callEndTime","next_trigger_date"]
        try:
            response = requests.request("POST", mongo_url, headers=headers, json=payload)
            res = json.loads(response.text)
            logger.debug("RES :"+str(response.text))
            if res['status']==404:
                logger.debug('Error from the server')
                return ""
            else:         
                return res["count"]
        except Exception as e:
            logger.exception("Exception in mongo_file_count api --> "+ str(e))
            client.captureException()
            return ""

    
    def mongo_delete(self,col_type="",q1={"phone_number":11111}):   
        ''' This function will return the true or false based on whether the record is deleted or not''' 
        if col_type == "input":
            COL = self.input_col
        elif col_type == "output" or col_type == "report":
            COL = self.output_col
        elif col_type == "voice_blast_report":
                COL = self.voice_blast_col
        else:
            logger.info(red+"Please mention the collection type input or output"+end)
            exit()
        mongo_url = self.mongo_url+"/"+"remove"
        logger.debug("URL:"+str(mongo_url))   
        payload={
                    "db":self._mongo_db,
                    "collection" : COL,
                    "dict_condition":q1
                }
        try:
            response = requests.request("POST", mongo_url, headers=headers, json=payload)
            res = json.loads(response.text)
            logger.debug("RES :"+str(response.text))
            if res['message']=="Record Does not Exist" and res['status']==200:
                logger.debug("Record Does not Exist")
                return False
            elif res['status']==404:
                logger.debug('Error from the server')
                return False
            else:
                logger.debug("Record is successfully deleted")
                return True
        except Exception as e:
            logger.exception("Exception in mongo_delete api --> "+ str(e)) 
            client.captureException()               
            return False

    # mode => insert or update ; record-> to insert or set fields; upd_cond-> In case of update condition
    # col_type -> input collection or output collection
    def mongo_update(self,col_type="",record={},upd_cond={"phone_number":11111}):
        ''' This function will return the true or false based on whether insert/update was performed on the database''' 
        if col_type == "input":
            COL = self.input_col
        elif col_type == "output" or col_type == "report":
            COL = self.output_col
        elif col_type == "voice_blast_report":
                COL = self.voice_blast_col
        else:
            logger.info(red+"Please mention the collection type input or output"+end)
            exit()
        mongo_url = self.mongo_url+"/"+"update"
        logger.debug("URL:"+str(mongo_url))
        li = ["last_triggered_date","callConnectedTime","callEndTime","next_trigger_date"]
        for k,v in record.items():
            if k in li and v!="":
                record[k] = v.__str__()
        payload={
                    "db":self._mongo_db,
                    "collection" : COL,
                    "dict_condition":upd_cond,
                    "dict_update": record
                }
    
        try:  
            response = requests.request("POST", mongo_url, headers=headers, json=payload)
            res = json.loads(response.text)
            logger.debug("RES :"+str(response.text))
            if res['message']=="Record Does not Exist" and res['status']==200:
                logger.debug("Record Does not Exist")
                return False
            elif res['status']==404:
                logger.debug('Error from the server')
                return False
            else:
                logger.debug("Record is successfully updated")
                return True
        except Exception as e:
            logger.exception("Exception in mongo_update api  --> "+ str(e))
            client.captureException()
            return False

    def mongo_insert(self,col_type="",record={}):
        ''' This function will return the true or false based on whether insert/update was performed on the database''' 
        if col_type == "input":
            COL = self.input_col
        elif col_type == "output" or col_type == "report":
            COL = self.output_col
        elif col_type == "voice_blast_report":
                COL = self.voice_blast_col
        else:
            logger.info(red+"Please mention the collection type input or output"+end)
            exit()
        mongo_url = self.mongo_url+"/"+"insert"
        logger.debug("URL:"+str(mongo_url))
        li = ["last_triggered_date","callConnectedTime","callEndTime","next_trigger_date"]
        for k,v in record.items():
            if k in li and v!="":
                record[k] = v.__str__()
        payload={
                    "db":self._mongo_db,
                    "collection" : COL,
                    "data": record
                }
        try:  
            response = requests.request("POST", mongo_url, headers=headers, json=payload)
            res = json.loads(response.text)
            logger.debug("RES :"+str(response.text))
            if res['status']==404:
                logger.debug('Error from the server')
                return False
            else:
                logger.debug("Record is successfully inserted")
                logger.info(res)
                return True
        except Exception as e:
            logger.exception("Exception in mongo_update api --> "+ str(e))
            client.captureException()
            return False
                        
    #col type => input or output ; q1 -> filter rows query ; q2-> filter column query if present 
    def mongo_get_collection_data(self,col_type, q1,q2={"_id": 0}):
        '''this function will return the document from the collection'''
        if col_type == "input":
            COL = self.input_col
        elif col_type == "output" or col_type == "report":
            COL = self.output_col
        elif col_type == "voice_blast_report":
                COL = self.voice_blast_col
        else:
            logger.info(red+"Please mention the collection type input or output"+end)
            exit()
        mongo_url = self.mongo_url+"/"+"findone"
        logger.debug("URL:"+str(mongo_url))
        logger.info("URL:"+str(mongo_url))
        payload={
                    "db":self._mongo_db,
                    "collection" : COL,
                    "dict_query": q1
                }
        try:
            response = requests.request("POST", mongo_url, headers=headers, json=payload)
            res = json.loads(response.text)
            logger.debug("RES :"+str(response.text))
            logger.info("RES :"+str(res))
            if res['message']=="Record Does not Exist" and res['status']==200:
                logger.debug('Record Does not Exist')
                return ""
            elif res['status']==404:
                logger.debug('Error from the server')
                return False
            else:
                logger.debug('Record Fetched Successfully')
                reterived_collection = res['result'] 
                li = ["last_triggered_date","callConnectedTime","callEndTime","next_trigger_date"]
                for k,v in reterived_collection.items():
                    if k in li and v!="":
                        reterived_collection[k] = datetime.strptime(v[0:19], '%Y-%m-%d %H:%M:%S')            
                return reterived_collection
        except Exception as e:
            logger.exception("Exception in mongo_get_collection_data api  --> "+ str(e))
            client.captureException()
            return ""


#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------
class RedisDB:
    
    def __init__(self):
        
        try:
            self._redis_config = load_config()
            environment = self._redis_config["server"]["environment"]
            if environment == "prod":
                # Reading from environment variables in case of Prod
                redis_host = os.environ["redis_host"]
                redis_port = os.environ["redis_port"]
            else:
                redis_host = self._redis_config["redis_tracker_store"]["url"]
                redis_port = self._redis_config["redis_tracker_store"]["port"]

            db_number = self._redis_config["redis_tracker_store"]["db_mapping"]
            db_tracker_num = self._redis_config["redis_tracker_store"]["db_tracker"]
            
            if (redis_host is not None) and (redis_port is not None):
                self._redis_conn = redis.StrictRedis(host=redis_host, port=redis_port, db=db_number)
                self._redis_track_conn = redis.StrictRedis(host=redis_host, port=redis_port, db=db_tracker_num)
                if self._redis_conn is not None and self._redis_track_conn is not None: 
                    logger.info("Successfully Connected to the Redis Database!!!")
                else:
                    logger.info(red+"Unable to connect Redis Database!!!"+red)
            else:
                logger.info(red+"Problem when fetching the Redis details from endpoints.yml !!!"+end) 
        except BaseException as e:
            logger.info(red+"ERROR!!! When Connecting to the Redis Database :  "+str(e)+end)   
            self._redis_conn = None
            self._redis_track_conn = None
    
    def redis_delete_tracker(self, key):
        """function to delete tracker in redis. 

        Args:
            key (string): key for which value has to delete

         Returns:
            integer: 0 or 1 based on whether key was deleted from redis or not
        """
        try:
            logger.info(blue+"Entered redis_delete_tracker function"+end)
            value = self._redis_track_conn.delete(key)
            logger.info(blue+"Exit redis_delete_tracker function"+end)
            return value
        except BaseException as e:
            logger.info(red+"Unable to DELETE value and expiry  for key : " + key + " ERROR is : "+str(e)+end)
    
        
    def redis_get_value(self,key):
        """function to get value in string form of a key from redis

        Args:
            key (string): key whose value has to be fetched

        Returns:
            string: value corresponding to the key set in redis
        """
        try:
            value = self._redis_conn.get(key)
            if value is None:
                return value
            else:
                return value.decode('utf-8')
        except BaseException as e:
            logger.info(red+"Unable to GET value for key : " + key + " ERROR is : "+str(e)+end)
    

    def redis_get_dict_value(self,key):
        """function to retrieve value which is in dictionary format for a given key in redis

        Args:
            key (string): Key whose value has to be fetched from redis

        Returns:
            dictionary: corresponding dictionary value of the key from redis
        """
        try:
            value = self._redis_conn.get(key)
            if value is None:
                return value
            else:
                return pickle.loads(value)
        except BaseException as e:
            logger.info(red+"Unable to GET value for key : " + key + " ERROR is : "+str(e)+end)


    def redis_set_value(self, key, value):
        """function to set a value for the key in redis. 
        Use this function if you want to set a string value to a key

        Args:
            key (string): key for which value has to be set
            value (string): value corresponding to the key

        Returns:
            integer: 0 or 1 based on value was set to corresponding key or not
        """
        try:
            value = self._redis_conn.set(key, value)
            return value
        except BaseException as e:
            logger.info(red+"Unable to SET value for key : " + key + " ERROR is : "+str(e)+end)
    
    def redis_set_dict_value(self, key, value):
        """function to set a value for the key in redis. 
        Use this function if you want to set a dictionary value to a key

        Args:
            key (string): key for which value has to be set
            value (dictionary): value corresponding to the key

        Returns:
            integer: 0 or 1 based on value was set to corresponding key or not
        """
        try:
            value = self._redis_conn.set(key, pickle.dumps(value))
            return value
        except BaseException as e:
            logger.info(red+"Unable to SET value for key : " + key + " ERROR is : "+str(e)+end)


    def redis_set_dict_value_and_expiry(self, key, value, expiry):
        """function to set a value for the key in redis. 
        Use this function if you want to set a dictionary value to a key

        Args:
            key (string): key for which value has to be set
            value (dictionary): value corresponding to the key

        Returns:
            integer: 0 or 1 based on value was set to corresponding key or not
        """
        try:
            value = self._redis_conn.set(key, pickle.dumps(value), ex=expiry)
            return value
        except BaseException as e:
            logger.info(red+"Unable to SET value for key : " + key + " ERROR is : "+str(e)+end)


    def redis_set_value_and_expiry(self, key, value, expiry):
        """function to set a value for the key in redis with expiry
       
        """
        try:
            value = self._redis_conn.set(key, value, ex=expiry)
            return value
        except BaseException as e:
            logger.info(red+"Unable to SET value and expiry for key : " + key + " ERROR is : "+str(e)+end)


    def redis_delete_value(self,key):
        """function to delete a key from redis

        Args:
            key (string): key which has to be deleted from redis

        Returns:
            integer: 0 or 1 based on whether key was deleted from redis or not
        """
        try:
            value = self._redis_conn.delete(key)
            return value
        except BaseException as e:
            logger.info(red+"Unable to DELETE value and expiry  for key : " + key + " ERROR is : "+str(e)+end)

    def get_details(self,tracker: Tracker):
        """function to fetch sender id and customer details dictionary from redis.
        Use this function in actions to get the sender id and customer details info from redis

        Args:
            tracker (Tracker): Tracker of the current Action

        Returns:
            string: sender id
            dictionary: customer information from redis which was set from mongo
        """
        try:
            sender_id = tracker.current_state()["sender_id"]
            # phone_number = self._redis_conn.get(sender_id)
            # cust_info = pickle.loads(self._redis_conn.get(phone_number))
            cust_info = pickle.loads(self._redis_conn.get(sender_id))
            # return the sender id and customer information dictionary
            return sender_id, cust_info
        
        except BaseException as e:
            sender_id = None
            cust_info = None
            logger.info(red+"Unable to details value :" + str(sender_id)+" ERROR is : "+str(e)+end)
            return sender_id,cust_info
    
    def update_stage(self,tracker: Tracker,dict_info={}):
        """function to update stage in actions
        Use this function to update the stage or other fields in actions

        Args:
            tracker (Tracker): Tracker of the current Action
            dict_info (dict, optional): dictionary containing fields that have to be updated in redis. Defaults to {}.

        Returns:
            boolean: True or False based on whether the updation was successfull or not
        """
        try:
            sender_id,cust_info = self.get_details(tracker)
            # phone_number = cust_info["phone_number"]
            updated_dict = {**cust_info, **dict_info}
            # self._redis_conn.set(phone_number, pickle.dumps(updated_dict))
            self._redis_conn.set(sender_id, pickle.dumps(updated_dict))
            return True
        
        except BaseException as e:
            logger.info(red+"update_stage: Exception!!! Failed to update to redis" + str(e)+end)
            return False


    def redis_key_confirm(self,key,rd_dict={}):
        """function to confirm if key is present in the redis or not

        Returns:
            boolean: True or False based on whether the updation was successfull or not
        """
        try:

            if key in rd_dict.keys():
                key_val = rd_dict[key]
                if key == "" or key is None:
                    key_val = ""                                 
            else:
                logger.info("******===========stage is not in redis data===========*****")
                key_val = ""

            return key_val
        
        except BaseException as e:
            logger.info(red+"redis_key_confirm: Exception!!! Failed to get data from redis" + str(e)+end)
            return False
    
    def tracker_details(self,tracker: Tracker,text, intent, confidence, action):
            """function to fetch tracker details for particular sesssion.

            Args:
                tracker (Tracker): Tracker of the current Action

            Returns:
                string: intent and confidence
            """
            try:
                sender_id,cust_info = self.get_details(tracker)
                logger.info(darkcyan+"[Intent Name:---> "+cyan+bold+str(intent)+darkcyan+" Confidence is :---> "+cyan+bold+str(confidence)+"]"+end)
                tracker_dict = {'action':action,'asr_text':text,'intent':intent,'confidence':confidence}
                cust_info['tracker_log'].append(tracker_dict)
                # update redis
                is_updated = self.update_stage(tracker,cust_info)
                if is_updated is True:
                    logger.info("Updated stage in redis tracker_details")
                else:
                    logger.info("failed to update in redis tracker_details")

                return "ok"
            
            except BaseException as e:
                logger.info("tracker_log key is not present")
                logger.info(red+"Unable to get tracker details :"+ str(sender_id)+" ERROR is : "+str(e)+end)
                return None
